#!/usr/bin/env python
# encoding: utf-8

import FlujoPrincipal
import sys

def fp():
    return FlujoPrincipal.FlujoPrincipal()

def main():

    if(len(sys.argv)<5):

        print('## Debe completar todas las variables ##')
        return
    else:
        CP = int(sys.argv[1])
        N = int(sys.argv[2])
        PD = int(sys.argv[3])
        ML = round(float(sys.argv[4]), 1)
        TF = 100*60*60*24*365

    fp().ejecutarConParametros(CP, N, PD, ML, TF)


if __name__ == '__main__':
    main()