#!/usr/bin/env python
# encoding: utf-8

import FlujoPrincipal
import sys

def fp():
    return FlujoPrincipal.FlujoPrincipal()

def main():

    datos = [
        [1200, 3, 20, 0.4],
        [1200, 3, 20, 0.7],
        [1200, 3, 30, 0.4],
        [1200, 3, 30, 0.7]
    ]

    TF = 100*60*60*24*365

    for row in datos:
        CP = int(row[0])
        N = int(row[1])
        PD = int(row[2])
        ML = round(float(row[3]), 1)

        print "Ejecutando simulacion: %s, %s, %s, %s" % (CP, N, PD, ML)
        fp().ejecutarConParametros(CP, N, PD, ML, TF)

if __name__ == '__main__':
    main()