#!/usr/bin/env python
# encoding: utf-8

import FlujoPrincipal
import sys

def fp():
    return FlujoPrincipal.FlujoPrincipal()

def main():

    print('Bienvenido!')

    print('')
    print('## Cálculo de Desperdicio de Jabón ##')
    print('')

    print('## Ingrese las variables de Control solicitadas ##')
    print('')
    CP = ingresarCP()

    print('')
    N = ingresarN()

    print('')
    PD = ingresarPD()

    print('')
    ML = ingresarML()

    print('')
    TF = ingresarTF()

    print('')
    print('## Ejecutando. Por favor espere... ##')
    print('')

    fp().ejecutarSinParametros(CP, N, PD, ML, TF)

    print('')
    print('## Simulación Finalizada ##')
    print('')

def ingresarCP():
    try:
        CP = input('Capacidad máxima de las bolsas de jabon (1200 o 1800 ml): ')
        if CP != 1200 and CP != 1800:
            raise ValueError
        return CP
    except (TypeError, NameError, ValueError, SyntaxError):
        print('El valor ingresado no es valido')
        return ingresarCP()

def ingresarN():
    try:
        N = input('Cantidad de dispositivos (valor entero mayor a cero): ')
        N = int(N)
        if N > 0:
            return N
        else:
            raise ValueError
    except (TypeError, NameError, ValueError, SyntaxError):
        print('El valor ingresado no es valido')
        return ingresarN()

def ingresarPD():
    try:
        PD = input('Porcentaje de jabón a desechar (valor entre 0 y 100): ')
        if PD < 0 or PD > 100:
            raise ValueError
        return PD
    except (TypeError, NameError, ValueError, SyntaxError):
        print('El valor ingresado no es valido')
        return ingresarPD()

def ingresarML():
    try:
        ML = input('Ml por uso del dispenser (0.4 o 0.7 ml): ')
        if ML != 0.4 and ML != 0.7:
            raise ValueError
        return ML
    except (TypeError, NameError, ValueError, SyntaxError):
        print('El valor ingresado no es valido')
        return ingresarML()

def ingresarTF():
    try:
        TF = input('Cantidad total de años a simular (valor entero mayor a cero): ')
        TF = int(TF)
        if TF > 0:
            return TF*60*60*24*365
        else:
            raise ValueError
    except (TypeError, NameError, ValueError, SyntaxError):
        print('El valor ingresado no es valido')
        return ingresarTF()

if __name__ == '__main__':
    main()