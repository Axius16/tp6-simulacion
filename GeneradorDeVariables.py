import random

class GeneradorDeVariables():

    def IA(self):
        r = random.uniform(0, 0.993)
        if r == 0 or r == 1:
            r = self.IA()
        IA = 2890.8*(1/((1/r)**(1/0.08257)+(-1)))**(1/3.9828)
        return int(round(IA, 0))

    def buscarDispenser(self, N):
        if(random.uniform(0, 1) < 0.65):
            return 0
        r = random.uniform(0, 1)
        m = int(round((N - 1) * r, 0))
        return m

    def PLLBJ(self):
        r = random.uniform(0, 1)
        PLLBJ = 1.001 - 0.25*(1-r)**(1/0.3217)
        return round(PLLBJ, 2)