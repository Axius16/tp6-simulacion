import GeneradorDeVariables


def generador():
    return GeneradorDeVariables.GeneradorDeVariables()

class FlujoPrincipal():


    def ejecutarConParametros(self, CP, N, PD, ML, TF):

        (RT, JDT, TBCT) = self.ejecutar(CP, N, PD, ML, TF)
        linea = str(CP) + ',' + str(N) + ',' + str(PD) + ',' + str(ML) + ',' + str(RT) + ',' + str(JDT) + ',' + str(TBCT)

        f = open ("ejecuciones.csv", "a")
        f.write(linea+"\n")
        f.close()


    def ejecutarSinParametros(self, CP, N, PD, ML, TF):

        (RT, JDT, TBCT) = self.ejecutar(CP, N, PD, ML, TF)
        self.imprimirResultados(CP, N, PD, ML, RT, JDT, TBCT)


    def ejecutar(self, CP, N, PD, ML, TF):
        # Definicion de las variables
        T = 0
        TBC = 0
        PJD = 0
        TLLD = 0
        TCD = float('inf') #valor 'infinito'
        DJ = 0
        CRT = 0
        RT = 0
        JDT = 0
        CJ = self.inicializarCJ(N, CP, PD, DJ)
        # Ejecucion
        while T < TF:
            if(TLLD < TCD):
                #DISPENSA
                T = TLLD
                IA = generador().IA()
                TLLD = T + IA
                i = generador().buscarDispenser(N)
                CJ[i] = round(CJ[i] - ML, 1)
                if CJ[i] < ML:
                    #PLANIFICA RECARGA
                    TCD = T
            else:
                #RECARGA
                TCD = float('inf')
                CRT = CRT + 1
                j = 0
                while j < N:
                    (DJ, TBC) = self.reponerDispenser(CJ, j, CP, PD, DJ, TBC)
                    j += 1

        #Calculo Resultados (RT, CRT, T, JDT, DJ)
        mes = 3600*24*30
        RT = round((float(CRT) * mes) / T , 2)
        JDT = round((DJ * mes) / T, 2)
        TBCT = round((float(TBC) * mes) / T, 2)

        return (RT, JDT, TBCT)


    def inicializarCJ(self, N, CP, PD, DJ):
        i = 0
        CJ = []
        while i < N:
            CJ.append(int(0))
            self.reponerDispenser(CJ, i, CP, PD, 0, 0)
            i += 1
        return CJ


    def reponerDispenser(self, CJ, j, CP, PD, DJ, TBC):
        if (CJ[j] / CP) * 100 < PD:
            DJ = DJ + CJ[j]
            TBC = TBC + 1
            PLLBJ = generador().PLLBJ()
            CJ[j] = int(CP * PLLBJ)
        return (DJ, TBC)


    def imprimirResultados(self, CP, N, PD, ML, RT, JDT, TBCT):
        print('')
        print('##########################')
        print('## Variables de Control ##')
        print('##########################')
        print('')
        print('Capacidad maxima de las bolsas de jabon en ml (CP):')
        print(CP)
        print('')
        print('Cantidad de dispositivos (N):')
        print(N)
        print('')
        print('Porcentaje de jabon a desechar (PD):')
        print(PD)
        print('')
        print('Ml por uso del dispenser (ML):')
        print(ML)
        print('')
        print('')
        print('############################')
        print('## Variables de Resultado ##')
        print('############################')
        print('')
        print('Promedio de Reposiciones por mes (RT):')
        print(RT)
        print('')
        print('Promedio de Jabon desechado por mes (JDT):')
        print(JDT)
        print('')
        print('Promedio de Bolsas cambiadas por mes (TBCT):')
        print(TBCT)
        print('')